$(document).ready(function(){
	console.log('ready');

	$('.thumb-list').slick({
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
	});

	$('.list-imovel.list-relac').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [{
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        arrows: false,
  			autoplay: true,
  			autoplaySpeed: 2500,
	      }
	    }]
	});

	$('.filter, #close-btn').on('click', function() {
		$('.aside-content').fadeToggle(300);
	});

	$('.aside-content h3').on('click', function() {
		if($(this).next('ul').is(':visible')) {
			$('h3 + ul').slideUp();
		} else {
			$('h3 + ul').slideUp();
			$(this).next('ul').slideDown('open');
		}
	});
});

function toggleFormat(f) {
	if(f == 'col') {
		$('.list-imovel').addClass('format-col').find('.thumb-list').slick('refresh');
	} else {
		$('.list-imovel').removeClass('format-col').find('.thumb-list').slick('refresh');
	}
}

function toggleInfoBox(e) {
	jQuery('.' + e + '-box').slideToggle(300);
}

// Filtra os resultados dos imóveis
function filter()
{
	// Serializa o form
	var form = $("#form-filter").serialize();

    // Pega a ordenação atual
    var order = $(".order").val();

	// Envia os filtros por ajax
	$.ajax({
		url: '/filtros-imoveis',
		type: 'POST',
		data: form+'&order='+order,
		beforeSend: function()
		{
			// Exibe o loading da página
			// alert('Aguarde');
		},
		success: function(response)
		{
			// Apaga o alerta Atual
			$("#alerta-error").remove();

			// Remove o total anterior
			$(".totalReturn").remove();

			// Remove a paginacao
			$("#paginacao").remove();

			// Remove os resultados atuais
			$(".list-item").remove();


			// Joga o resultado na div
			$("#conteudo-listagem").append(response);

            // Insere o total de imoveis encontrados na pesquisa
            var total = $(".totalReturn").val();
            $("#total_encontrados").html(total);
		}
	});
}

function goToPage(page)
{
    // Serializa o form
    var form = $("#form-filter").serialize();

    // Pega a ordenação atual
    var order = $(".order").val();

    // Envia os filtros por ajax
    $.ajax({
        url: '/filtros-imoveis?page='+page,
        type: 'POST',
        data: form+'&order='+order,
        beforeSend: function()
        {
            // Exibe o loading da página
            // alert('Aguarde');
        },
        success: function(response)
        {
            // Apaga o alerta Atual
            $("#alerta-error").remove();

            // Remove o total anterior
            $(".totalReturn").remove();

            // Remove a paginacao
            $("#paginacao").remove();

            // Remove os resultados atuais
            $(".list-item").remove();


            // Joga o resultado na div
            $("#conteudo-listagem").append(response);

            // Insere o total de imoveis encontrados na pesquisa
            var total = $(".totalReturn").val();
            $("#total_encontrados").html(total);
        }
    });
}


function changeOrder(order)
{
	// Serializa o form
    var form = $("#form-filter").serialize();

    // Envia os filtros por ajax
    $.ajax({
        url: '/filtros-imoveis',
        type: 'POST',
        data: form+'&order='+order,
        beforeSend: function()
        {
            // Exibe o loading da página
            // alert('Aguarde');
        },
        success: function(response)
        {
            // Apaga o alerta Atual
            $("#alerta-error").remove();

            // Remove o total anterior
            $(".totalReturn").remove();

            // Remove a paginacao
            $("#paginacao").remove();

            // Remove os resultados atuais
            $(".list-item").remove();


            // Joga o resultado na div
            $("#conteudo-listagem").append(response);

            // Insere o total de imoveis encontrados na pesquisa
            var total = $(".totalReturn").val();
            $("#total_encontrados").html(total);
        }
    });
}