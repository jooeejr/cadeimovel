<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/imovel/{id}', 'ImoveController@index')->name('imovel');
Route::get('/imoveis', 'ImoveController@lista')->name('imovel.lista');

// Lista com a busca por região
Route::get('/busca-imoveis/{regiao}', 'ImoveController@listaRegiao')->name('imovel.regiao');

// Lista todos os imoveis na página
Route::post('/imoveis', 'ImoveController@RedirecionamentoLista')->name('imovel.lista.regiao');

// Faz o filtro dos imóveis
Route::post('/filtros-imoveis', 'ImoveController@filter')->name('filtro.imoveis');


Route::get('/dashboard/imovel', 'Dashboard\ImovelController@index')->name('dashboard.imovel');
Route::post('/dashboard/imovel', 'Dashboard\ImovelController@save')->name('dashboard.imovel.cadastro');
Route::get('/dashboard/imovel/{id}', 'Dashboard\ImovelController@findImovel')->name('dashboard.imovel.find');
Route::post('/dashboard/update-imovel/{id}', 'Dashboard\ImovelController@update')->name('dashboard.imovel.update');
Route::delete('/dashboard/imovel/apagar/{id}', 'Dashboard\ImovelController@apagar')->name('dashboard.imovel.apagar');

Route::get('/dashboard/usuario', 'Dashboard\UsuarioController@index')->name('dashboard.usuario');
Route::get('/dashboard/usuario/{id}', 'Dashboard\UsuarioController@findUser')->name('dashboard.usuario.find');
Route::post('/dashboard/usuario/update', 'Dashboard\UsuarioController@update')->name('dashboard.usuario.update');
Route::delete('/dashboard/usuario/apagar/{id}', 'Dashboard\UsuarioController@apagar')->name('dashboard.usuario.apagar');