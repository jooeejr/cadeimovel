<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImoveisPhoto extends Model
{
    protected $fillable = ['imovel_id', 'filename'];

    public function imovel()
    {
        return $this->belongsTo('App\Imovel');
    }
}
