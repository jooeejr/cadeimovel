<?php

namespace App\Http\Controllers\Dashboard;

use App\ImoveisPhoto;
use App\Imovel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;



class ImovelController extends Controller
{
    /**
     * HomeDashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index()
    {
        $imoveis = Imovel::paginate('15');

        return view('dashboard.imovel', compact('imoveis'));
    }

    function save(Request $request)
    {
        //TODO: CRIAR VALIDAÇÃO POR TAMANHO DE IMAGEM
        $imovel = new Imovel();
        $this->preencheObjeto($imovel, $request->request->all());

        $imovel->save();
        $photos = $request->files->all()['photos'];

        foreach($photos as $index => $p) {
            $imovelPhoto = new ImoveisPhoto();
            $imovelPhoto->imovel_id = $imovel->id;
            $imovelPhoto->filename = "";
            $imovelPhoto->filename = $request->file('photos')[$index]->store('imagens', 'public');
            $imovelPhoto->save();
        }

        return redirect()->route('dashboard.imovel');
    }

    function findImovel($id)
    {
        $imovel = Imovel::find($id);
        if (asset($imovel)) {
            return json_encode($imovel);
        }
        return response('Imovel Não Encontrado', 403);
    }

    /**
     * @param $id
     * @return false|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|string
     */
    function apagar($id)
    {
        $imovel = Imovel::find($id);
        if (asset($imovel)) {
            $imoveis_photos = $imovel->imoveis_photos()->getResults();
            foreach ($imoveis_photos as $imovei_photo) {
                Storage::disk('public')->delete($imovei_photo->filename);
                $imovei_photo->delete();
            }
            $isSucess = $imovel->delete();
            $mensagem = ["estaApagado" => $isSucess];
            return json_encode($mensagem);
        }
        return response('Imovel Não Encontrado', 403);
    }


    public function update(Request $request, $id)
    {
        $up = Imovel::find($id);

        // Se não encontrar o registro, redireciona para a pagina de cadastro
        if(empty($up))
        {
            return redirect('/dashboard/imovel');
        }

        // Caso encontre o registro, faz a atualização das informações
        $up->titulo = $request->input('titulo');
        $up->descricao = $request->input('descricao');
        $up->preco = $request->input('preco');
        $up->condominio = $request->input('condominio');
        $up->iptu = $request->input('iptu');
        $up->area = $request->input('area');
        $up->tipo_imovel = $request->input('tipo_imovel');
        $up->operacao = $request->input('operacao');
        $up->status = $request->input('status');
        $up->cep = $request->input('cep');
        $up->bairro = $request->input('bairro');
        $up->endereco = $request->input('endereco');
        $up->complemento = $request->input('complemento');
        $up->numero = $request->input('numero');
        $up->estado = $request->input('estado');
        $up->cidade = $request->input('cidade');
        $up->telefone = $request->input('telefone');
        $up->link_360 = $request->input('link_360');
        $up->email = $request->input('email');
        $up->qtd_quartos = $request->input('qtd_quartos');
        $up->qtd_banheiros = $request->input('qtd_banheiros');
        $up->qtd_vaga_garagem = $request->input('qtd_vaga_garagem');
        $up->save();



        if(!empty($request->file('photos')))
        {
            // Se recebeu alguma requisição, faz a atualização das fotos
            $photos = $request->file('photos');

            // Deleta as fotos anteriores
            $fotosImoveis = ImoveisPhoto::select('filename')
                ->where('imovel_id', '=' ,$id)->get();

            foreach ($fotosImoveis as $imovei_photo) {
                Storage::disk('public')->delete($imovei_photo->filename);

                ImoveisPhoto::where('imovel_id', '=' ,$id)->delete();
            }



            // Upa as novas
            foreach($photos as $index => $p) {
                $search = new ImoveisPhoto();
                $search->imovel_id = $id;
                $path = $request->file('photos')[$index]
                    ->store('imagens', 'public');
                $search->filename = $path;
                $search->save();
            }

        }

        // Mantém as fotos e apenas redireciona a página
        return redirect('/dashboard/imovel');
    }

}