<?php

namespace App\Http\Controllers\Dashboard;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsuarioController extends Controller
{
    /**
     * HomeDashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){
        $users = User::all();
        return view('dashboard.usuario', compact('users'));
    }

    function findUser($id){
        $user = User::find($id);
        if(asset($user)){
            return json_encode($user);
        }
        return response('Usuario Não Encontrado', 403);
    }

    function update(Request $request){
        $user = User::find($request->input('id'));
        if(asset($user)){
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->save();
            return redirect()->route('dashboard.usuario');
        }
        return response('Usuario Não Encontrado', 403);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    function apagar($id){
        $user = User::find($id);
        if(asset($user)){
            $isSucess = $user->delete();
            $mensagem = ["estaApagado" => $isSucess];
            return json_encode($mensagem);
        }
        return response('Usuario Não Encontrado', 403);
    }
}