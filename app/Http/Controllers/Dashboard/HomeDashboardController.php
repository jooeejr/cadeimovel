<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeDashboardController extends Controller
{

    /**
     * HomeDashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){
        return view('dashboard.home');
    }
}
