<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imovel;
use Illuminate\Support\Facades\DB;

class ImoveController extends Controller
{

    public function __construct()
    {

    }

    public function index($id)
    {
        $imovel = Imovel::with('imoveis_photos')
        ->find($id);
        return view('imovel.index', compact('imovel'));
    }

    public function lista()
    {
        $imoveis = Imovel::with('imoveis_photos')
            ->paginate(10);
        return view('imovel.lista_imovel', compact('imoveis'));
    }

    public function RedirecionamentoLista(Request $request)
    {
        $regiao = $request->input('local');

        return redirect('/busca-imoveis/'.$regiao);
    }

    public function listaRegiao($regiao)
    {
        $imoveis = Imovel::with('imoveis_photos')
            ->where('bairro', 'like' ,'%'.$regiao.'%')
            ->orWhere('cidade', 'like', '%'.$regiao.'%')
            ->orWhere('estado', 'like', '%'.$regiao.'%')
            ->orWhere('titulo', 'like', '%'.$regiao.'%')
            ->paginate(10);
        return view('imovel.lista_imovel_regiao', compact('imoveis'));

    }


    public function filter(Request $request)
    {
        $sql = Imovel::with('imoveis_photos');

        if ($request->has('localidade'))
        {

            $localidade = $request->input('localidade');


                $sql->where(function($q) use ($localidade) {
                    $q->where('bairro', 'like', '%'.$localidade.'%')
                    ->orWhere('cidade', 'like', '%'.$localidade.'%')
                    ->orWhere('estado', 'like', '%'.$localidade.'%')
                    ->orWhere('titulo', 'like', '%'.$localidade.'%');
            });
        }



        // Se o tipo foi setado
        if ($request->has('tipo'))
        {
            $tipo_imovel = $request->input('tipo');
            $sql->whereIn('tipo_imovel', $tipo_imovel);
        }

        // Caso o status seja enviado, filtra por ele
        if ($request->has('status')) {
            $status = $request->input('status');
            $sql->whereIn('status', $status);
        }

        // Caso o minimo seja setado
        if (!empty($request->input('min'))) {
            $min = $request->input('min');
            $sql->where('preco', '>=', $min);
        }

        // Caso o Mázimo seja setado
        if (!empty($request->input('max'))) {
            $max = $request->input('max');
            $sql->where('preco', '<=', $max);
        }

        // Caso o numero de quartos seja setado
        if ($request->has('qtd_quartos')) {
            $qtd_quartos = $request->input('qtd_quartos');
            $sql->where('qtd_quartos', '>=', $qtd_quartos);
        }

        // Caso o numero de vagas seja setado
        if ($request->has('qtd_vagas')) {
            $qtd_vagas = $request->input('qtd_vagas');
            $sql->where('qtd_vaga_garagem', '>=', $qtd_vagas);
        }

        // Busca a palavra chave dentro da descricao
        if ($request->has('keyword')) {
            $keyword = $request->input('keyword');
            $sql->where('descricao', 'LIKE' ,'%'.$keyword.'%');
        }

        // Verifica se houve filtro por ordenacao
        if ($request->has('order')) {
            $order = $request->input('order');

            switch ($order){
                case 'best-price':
                    $sql->orderBy('preco', 'desc');
                    break;

                case 'lower-price':
                    $sql->orderBy('preco', 'asc');
                    break;

                case 'recent':
                    $sql->orderBy('created_at', 'desc');
                    break;

                case 'recent':
                    $sql->orderBy('created_at', 'desc');
                    break;
            }


        }



        // Retorna a query
        $data = $sql->paginate(10);


        // Total de resultados
        $total = count($data);

        if ($total > 0) {

            $cont = 0;
            foreach ($data AS $im) {
        ?>
            <div class="list-item">
                    <div class="imovel-content">
                        <div class="container-fluid">
                            <div class="format-item row">
                                <div class="col-5 px-1">
                                    <div class="relative">
                                        <div class="thumb-list">

                                            <?php
                                                foreach ($data[$cont]->imoveis_photos AS $photo) {
                                            ?>
                                                <a href="<?= route('imovel', $im->id) ?>" class="thumb main-thumb">
                                                        <div class="src"
                                                             style="background-image: url('<?= asset('/storage/') . '/' . $photo->filename ?>');"></div>
                                                </a>
                                            <?php
                                                }
                                            ?>
                                        </div>
                                        <span class="thumb-tag">Destaque!</span>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <a href="<?= route('imovel', $im->id) ?>">
                                        <h2><?= $im->titulo ?></h2>

                                        <div class="address"><?= $im->endereco ?>, <?= $im->numero ?>
                                            - <?= $im->bairro ?> - <?= $im->cidade ?></div>

                                        <ul class="list-inline">
                                            <li class="list-inline-item"><strong><?= $im->qtd_quartos ?></strong> <span>Quartos</span>
                                            </li>
                                            <li class="list-inline-item"><strong><?= $im->qtd_banheiros ?></strong>
                                                <span>Banheiro</span></li>
                                            <li class="list-inline-item"><strong><?= $im->qtd_vaga_garagem ?></strong>
                                                <span>Vaga</span></li>
                                        </ul>

                                        <div class="description">
                                            <p><?= $im->descricao ?></p>
                                        </div>
                                    </a>

                                    <div class="bottom">
                                        <div class="container-fluid">
                                            <div class="row justify-content-between align-items-center">
                                                <div class="col-auto format-nopad">
                                                    <p class="price">A partir de
                                                        <br><strong>R$ <?= $im->preco ?></strong></p>
                                                </div>
                                                <div class="col-auto format-nopad">
                                                    <a href="<?= route('imovel', 1) ?>" class="btn">Telefone</a>
                                                    <a href="<?= route('imovel', 1) ?>" class="btn">Mensagem</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
                $cont++;
            }
        ?>
            <style>
                .pagination-style
                {
                    font-size: 13px;
                    font-family: "Open Sans", sans-serif;
                    font-weight: 600;
                    display: block;
                    color: #eb641c;
                    line-height: 2.4;
                    padding: 0 12px;
                    border: 1px solid #eb641c;
                    -webkit-transition: .2s all ease;
                    transition: .2s all ease;
                }

                .pagination-style:hover
                {
                    color: #fff;
                    background-color: #eb641c;
                }

                .slick-list {
                    position: relative;
                    overflow: hidden;
                    display: block;
                    margin: 0;
                    padding: 0;
                    width: 374px;
                }
            </style>

            <script type="text/javascript">
                $(document).ready(function(){
                    console.log('ready');

                    $('.thumb-list').slick({
                        infinite: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    });

                    $('.list-imovel.list-relac').slick({
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        responsive: [{
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: false,
                                autoplay: true,
                                autoplaySpeed: 2500,
                            }
                        }]
                    });

                    $('.filter, #close-btn').on('click', function() {
                        $('.aside-content').fadeToggle(300);
                    });

                    $('.aside-content h3').on('click', function() {
                        if($(this).next('ul').is(':visible')) {
                            $('h3 + ul').slideUp();
                        } else {
                            $('h3 + ul').slideUp();
                            $(this).next('ul').slideDown('open');
                        }
                    });
                });
            </script>
            <?php
                // Verifica se há mais de 10 itens para exibir a paginação
            ?>
            <input class='totalReturn' type='hidden' id='total-return' value='<?= $data->total() ?>'>
            <?php
                if ($data->total() > 10) {
                    ?>
                    <div class="col-md-12">
                        <div id="paginacao" class="pagger-list pull-right">
                            <ul class="pagination" role="navigation">
                                <?php
                                // Verifica se é a primeira página
                                if ($data->currentPage() == 1) {
                                    $valFirst = "disabled";
                                    $last = "";
                                } else {
                                    $lastPage = $data->currentPage() - 1;
                                    $valFirst = "";
                                    $last = "onClick='goToPage(" . $lastPage . ")'";
                                }
                                ?>
                                <li <?= $last ?> class="page-item <?= $valFirst ?>" aria-disabled="true"
                                                 aria-label="« Anterior">
                                    <span class="page-link pagination-style" aria-hidden="true">Anterior</span>
                                </li>
                                <?php
                                // Monta a paginação
                                $totalPages = $data->total() / 10;

                                // Ajusta o total de paginas
                                $sep = explode('.', $totalPages);

                                if (!empty($sep[1]))
                                {
                                    $totalPages += 1;
                                }

                                for ($i = 1; $i <= $totalPages; $i++) {




                                    // Verifica se o botão esta ativo
                                    if ($i == $data->currentPage()) {
                                        $selecao = "active";
                                    } else {
                                        $selecao = "";
                                    }
                                    ?>
                                    <li onClick="goToPage(<?= $i ?>)" class="page-item <?= $selecao ?>"
                                        aria-current="page">
                                        <span class="page-link pagination-style"><?= $i ?></span>
                                    </li>
                                    <?php
                                }

                                // Verifica se é a ultima página
                                if ($data->lastPage() == $data->currentPage()) {
                                    $valLast = "disabled";
                                    $next = "";
                                } else {
                                    // Soma a pagina atual para +1 pagina a frente
                                    $nextPage = $data->currentPage() + 1;
                                    $valLast = "";
                                    $next = "onClick='goToPage(" . $nextPage . ")'";
                                }
                                ?>
                                <li class="page-item <?= $valLast ?>">
                                    <span <?= $next ?> class="page-link pagination-style"
                                                       aria-label="Próximo »">Próximo</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php
                }
        }else{
            echo "<input class='totalReturn' type='hidden' id='total-return' value='".$total."'>";
            echo "<div id='alerta-error' class='alert alert-warning'>
                  <strong>Ooooops!</strong> Nenhum resultado foi encontrado.
                </div>";
        }



    }


}
