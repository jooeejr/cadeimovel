<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImoveisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imoveis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->text('descricao');
            $table->double('preco');
            $table->string('operacao');
            $table->string('tipo_imovel');
            $table->string('status');
            $table->string('cep');
            $table->string('bairro');
            $table->string('endereco');
            $table->string('complemento');
            $table->string('numero');
            $table->double('condominio');
            $table->double('iptu');
            $table->double('area');
            $table->string('estado');
            $table->string('cidade');
            $table->string('link_360');
            $table->string('telefone');
            $table->string('email');
            $table->integer('qtd_quartos');
            $table->integer('qtd_banheiros');
            $table->integer('qtd_vaga_garagem');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imoveis');
    }
}
