@extends('layouts.dashboard')
@section('conteudo')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Usuario</li>
    </ol>

    <!-- Page Content -->
    <h1>Usuarios</h1>
    <hr>
    <div class="row">
        <div class="col-5">

            <div class="card">
                <div class="card-header">Cadastro de Usuários</div>

                <div class="card-body">
                    <form id="form-usuario" method="POST" action="{{ route('register') }}">
                        @csrf
                        <input class="campo" type="hidden" id="id" name="id">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nome</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                       class="campo form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       name="name"
                                       value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class=" invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Endereço de E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email"
                                       class="campo form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       name="email"
                                       value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Senha</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                       class=" form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar
                                Senha</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button id="btn-form" type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-7">
            <div class="card">
                <div class="card-header">Usuários Cadastrados</div>

                <div class="card-body">
                    @if(count($users) > 0)
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Email</th>
                                <th scope="col">Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th scope="row">{{$user->id}}</th>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>
                                        <button type="button"
                                                onclick="editarUsuario('{{route("dashboard.usuario.find", $user->id)}}')"
                                                class="btn btn-sm btn-primary">Editar
                                        </button>
                                        <button type="button"
                                                onclick="apagarUsuario('{{route("dashboard.usuario.apagar", $user->id)}}')"
                                                class="btn btn-sm btn-danger">Apagar
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function editarUsuario(url) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                type: 'GET',
                success: function (result) {
                    user = JSON.parse(result);
                    inputs = document.querySelectorAll('.campo');

                    $.each(inputs, function (data) {
                        inputs[data].value = user[inputs[data].id];

                    });
                    $('#form-usuario').attr('action', '{{route('dashboard.usuario.update')}}');
                    $('#btn-form').html('Editar');

                },
                error: function (result) {
                    console.log(result);
                    alert("Ocorreu um erro ao buscar o usuário");
                }
            });
        }

        function apagarUsuario(url) {
            var resposta = confirm("Tem Certeza que deseja apagar o Usuario ?");
            if (resposta === true) {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    type: 'DELETE',
                    success: function (result) {
                        console.log(result);
                        //window.location.reload();
                    },
                    error: function (result) {
                        console.log(result);
                        alert("Ocorreu um erro ao apagar o usuário");
                    }
                });
            }
        }

    </script>
@endsection