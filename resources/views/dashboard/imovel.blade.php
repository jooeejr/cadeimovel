@extends('layouts.dashboard')
@section('conteudo')
    <style>
        .pagination
        {
            float: right;
        }
    </style>

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route("dashboard.imovel")}}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Imovel</li>
    </ol>

    <!-- Page Content -->
    <h1>Cadastro de Imovel</h1>

    <div class="row">
        <div class="col-7">
            <form id="form-imovel" method="POST" action="" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="titulo">Titulo</label>
                            <input value= "Titulo do Imóvel" type="text" required name="titulo" class="campo form-control" id="titulo"
                                   placeholder="Titulo">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="descricao">Descrição</label>
                            <textarea class="campo form-control" name="descricao" required
                                      id="descricao"
                                      rows="3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere fugiat fugit, hic officia officiis quis voluptatem. Atque expedita facilis labore laudantium maxime odit, quae, quia repellat temporibus tenetur, ullam vero.</textarea>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="preco">Preço</label>
                            <input value= "123" type="number" name="preco" class="campo form-control" required id="preco"
                                   placeholder="Preço">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="condominio">Condominio</label>
                            <input value= "123" type="number" name="condominio" class="campo form-control" required id="condominio"
                                   placeholder="Condominio">
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label for="iptu">IPTU</label>
                            <input value= "123" type="number" name="iptu" class="campo form-control" id="iptu" required
                                   placeholder="IPTU">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="Area">Area</label>
                            <input value= "123" type="number" name="area" class="campo form-control" id="area" required
                                   placeholder="Area">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="tipo">Tipo Imovel</label>
                            <select class="campo form-control" name="tipo_imovel" required id="tipo">
                                <option value="Casa">Casa</option>
                                <option value="Apartamento">Apartamento</option>
                                <option value="Comercial">Comercial</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="status">Operação</label>
                            <select class="campo form-control" id="operacao" required name="operacao">
                                <option value="Aluguel">Aluguel</option>
                                <option value="A Venda">A Venda</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="status">Status Imovel</label>
                            <select class="campo form-control" id="status" required name="status">
                                <option value="Na Planta">Na Planta</option>
                                <option value="Em Construção">Em Construção</option>
                                <option value="Pronto para morar">Pronto para morar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="cep">CEP</label>
                            <input value= "123" type="text" required class="campo form-control" id="cep" name="cep"
                                   placeholder="CEP">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="bairro">Bairro</label>
                            <input value= "123" type="text" class="campo form-control" required name="bairro" id="bairro"
                                   placeholder="Bairro">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="endereco">Endereco</label>
                            <input value= "123" type="text" class="campo form-control" required name="endereco" id="endereco"
                                   placeholder="Endereco">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="complemento">Complemento</label>
                            <input value= "123" type="text" class="campo form-control" required name="complemento" id="complemento"
                                   placeholder="Complemento">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label for="numero">Numero</label>
                            <input value= "123" type="text" required class="campo form-control" name="numero" id="numero"
                                   placeholder="Numero">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="cidade">Estado</label>
                            <input value= "123" type="text" class="campo form-control" required id="estado" name="estado"
                                   placeholder="Estado">
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <input value= "123" type="text" class="campo form-control" id="cidade" required name="cidade"
                                   placeholder="Cidade">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="telefone">Telefone</label>
                            <input value= "123" type="text" class="campo form-control" id="telefone" required name="telefone"
                                   placeholder="Telefone">
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="form-group">
                            <label for="link_360">Link 360</label>
                            <input value= "123" type="text" class="form-control" id="link_360" required name="link_360"
                                   placeholder="Link 360">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input value= "teste@teste.com" type="email" class="form-control" id="email" name="email" required
                                   placeholder="Email">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="qtd_quartos">Qtd. de Quartos</label>
                            <input value= "123" type="number" class="campo form-control" name="qtd_quartos" required id="qtd_quartos"
                                   placeholder="Quantidade de Quartos">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="qtd_banheiros">Qtd. de Banheiros</label>
                            <input value= "123" type="number" class="campo form-control" name="qtd_banheiros" required
                                   id="qtd_banheiros"
                                   placeholder="Quantidade de Banheiros">
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label for="qtd_vaga_garagem">Qtd. de Vaga de Garagem</label>
                            <input value= "123" type="number" class="campo form-control" id="qtd_vaga_garagem"
                                   name="qtd_vaga_garagem" required
                            placeholder="Quantidade de Vaga de Garagem">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="send_imagens">Envie Imagens</label>
                            <input value= "123" type="file" class="campo form-control-file" name="photos[]" required
                                   id="send_imagens" multiple="multiple">
                        </div>
                    </div>
                </div>
                <button id="btn-form" class="btn btn-primary">Salvar Imovel</button>
            </form>
        </div>


        <div class="col-5">
            <table id="myTable" class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Titulo</th>
                    <th scope="col">Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($imoveis as $imovel)
                    <tr>
                        <th scope="row">{{$imovel->id}}</th>
                        <th>{{$imovel->titulo}}</th>
                        <td>
                            <button type="button"
                                    onclick="editarImovel('{{route("dashboard.imovel.find", $imovel->id)}}')"
                                    class="btn btn-sm btn-primary">Editar
                            </button>
                            <button type="button"
                                    onclick="apagarImovel('{{route("dashboard.imovel.apagar", $imovel->id)}}')"
                                    class="btn btn-sm btn-danger">Apagar
                            </button>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            <div class="col-md-12 pull-right">
                {{ $imoveis->links() }}
            </div>

        </div>
    </div>

@endsection
@section('scripts')
    <script>
        function editarImovel(url) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                dataType: 'json',
                type: 'GET',
                success: function (result) {
                    $("#titulo").val(result.titulo);
                    $("#descricao").val(result.descricao);
                    $("#preco").val(result.preco);
                    $("#condominio").val(result.condominio);
                    $("#iptu").val(result.iptu);
                    $("#area").val(result.area);
                    $("#tipo").val(result.tipo_imovel);
                    $("#operacao").val(result.operacao);
                    $("#status").val(result.status);
                    $("#cep").val(result.cep);
                    $("#bairro").val(result.bairro);
                    $("#endereco").val(result.endereco);
                    $("#complemento").val(result.complemento);
                    $("#numero").val(result.numero);
                    $("#estado").val(result.estado);
                    $("#cidade").val(result.cidade);
                    $("#telefone").val(result.telefone);
                    $("#link_360").val(result.link_360);
                    $("#email").val(result.email);
                    $("#qtd_quartos").val(result.qtd_quartos);
                    $("#qtd_banheiros").val(result.qtd_banheiros);
                    $("#qtd_vaga_garagem").val(result.qtd_vaga_garagem);


                    $('#form-imovel').attr('action', '/dashboard/update-imovel/'+result.id);
                    $('#btn-form').html('Editar');

                },
                error: function (result) {
                    console.log(result);
                    alert("Ocorreu um erro ao buscar o usuário");
                }
            });
        }

        function apagarImovel(url) {
            var resposta = confirm("Tem Certeza que deseja apagar o Imovel ?");
            if (resposta === true) {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    type: 'DELETE',
                    success: function (result) {
                        console.log(result);
                        window.location.reload();
                    },
                    error: function (result) {
                        console.log(result);
                        alert("Ocorreu um erro ao apagar o usuário");
                    }
                });
            }
        }



    </script>
@endsection