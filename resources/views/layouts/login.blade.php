<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Imoveis">
    <meta name="author" content="CadeImovel">

    <title>Painel Administrativo</title>

    <!-- Bootstrap core CSS-->
    <link rel="stylesheet" href="{{asset('node_modules/bootstrap/dist/css/bootstrap.css')}}">

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="{{asset('node_modules/font-awesome/css/font-awesome.css')}}">

    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="{{asset('css/dashboard/sb-admin.css')}}">

</head>

<body class="bg-dark">
@yield('content')

<!-- Bootstrap core JavaScript-->
<script src="{{asset('node_modules/jquery/dist/jquery.js')}} "></script>
<script src="{{asset('node_modules/bootstrap/dist/js/bootstrap.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('js/dashboard/sb-admin.js')}}"></script>

</body>

</html>
