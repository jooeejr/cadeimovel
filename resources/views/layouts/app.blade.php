<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0"/>
    <title>Template base - Gulp</title>
    <link rel="stylesheet" type="text/css" href="{{asset('node_modules/bootstrap/dist/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">


</head>
<body class="@yield('class_body')">
<header>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row justify-content-between align-items-center">
                <div class="col-5 col-sm-auto pr-1">
                    <h1 id="branding">
                        <a href="{{route('home')}}">
                            <img src="{{asset('img/logo-orange.png')}}"/>
                        </a>
                    </h1>
                </div>
                <div class="col-7 col-sm-auto pl-1">
                    <nav>
                        <ul class="nav justify-content-end">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('home')}}">Início</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('imovel.lista')}}">Imóveis</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

<main>
    @yield('content')
</main>

<footer>
    <div class="text-center">
        <span class="">2018 ©‎ Cadê Imóvel - Todos os Direitos</span>
    </div>
</footer>

<script type="text/javascript" src="{{asset('node_modules/jquery/dist/jquery.js')}} "></script>
<script type="text/javascript" src="{{asset('node_modules/bootstrap/dist/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('js/slick.js')}}"></script>
<script type="text/javascript" src="{{asset('js/script.js')}}"></script>
</body>
</html>