@extends('layouts.app')
@section('class_body')
    imoveis
@endsection
@section('content')
    <div id="app">
        <div class="bread-list">
            <div class="wrapper">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="{{route('home')}}">Início</a></li>
                    <li class="list-inline-item"><a href="{{route('imovel.lista')}}">Imóveis</a></li>
                    <li class="list-inline-item">Recreio dos bandeirantes</li>
                </ul>
            </div>
        </div>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="list-imovel row">
                    <div class="list-item list-single">
                        <div class="imovel-content">
                            <div class="container-fluid">

                                <div class="row">
                                    <div class="offset-0 offset-sm-1 col-12 col-sm-10">
                                        <div class="imovel-title">
                                            <h2 class="">{{ $imovel->titulo }}</h2>
                                            <div class="address">{{ $imovel->endereco }}, {{ $imovel->numero }} - {{ $imovel->bairro }}, {{ $imovel->cidade }} - {{ $imovel->estado }}</div>
                                        </div>
                                    </div>

                                    <div class="offset-0 offset-sm-1 col-12 col-sm-7 width-col-sm-7">
                                        <div class="relative">
                                            <div class="thumb-list">
                                                @foreach($imovel->imoveis_photos AS $foto)
                                                    <div class="thumb main-thumb">
                                                        <div class="src" style="background-image: url('{{asset('/storage/'.$foto->filename)}}');"></div>
                                                    </div>
                                                @endforeach
                                                {{--<div class="thumb main-thumb">--}}
                                                    {{--<div class="src" style="background-image: url('{{asset('img/imovel.jpg')}}');"></div>--}}
                                                {{--</div>--}}
                                            </div>
                                            <span class="thumb-tag">Alguma tag!</span>
                                        </div>
                                        <div class="description">
                                            <h3>Descrição</h3>
                                            <p>{{ $imovel->descricao }}</p>

                                            <h3>Características</h3>
                                            <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

                                            <ul class="list-point">
                                                <li>quis nostrud exercitation</li>
                                                <li>ullamco laboris</li>
                                                <li>quis nostrud exercitation ullamco laboris nisi</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-3">
                                        <div class="imovel-aside">
                                            <ul class="light-card list-price">
                                                <li class="list-price-item big"><small>{{ $imovel->operacao }}</small> <br><strong>R$ {{ $imovel->preco }}</strong></li>
                                                <li class="list-price-item"><small>Condomínio</small> <br><strong>R$ {{ $imovel->condominio }}</strong></li>
                                                <li class="list-price-item"><small>IPTU</small> <br><strong>R$ {{ $imovel->iptu }}</strong></li>
                                            </ul>

                                            <ul class="list-info light-card">
                                                <li class="list-info-item"><small>Tipo de imóvel</small> <br><strong>{{ $imovel->tipo_imovel }}</strong></li>
                                                <li class="list-info-item"><small>Área</small> <br><strong>{{ $imovel->area.'m²' }}  (R$ {{ number_format($imovel->preco/$imovel->area, '2') }}/m²)</strong></li>
                                                <li class="list-info-item"><strong>{{ $imovel->qtd_quartos }}</strong> <span>Quartos</span></li>
                                                <li class="list-info-item"><strong>{{ $imovel->qtd_banheiros }}</strong> <span>Banheiro</span></li>
                                                <li class="list-info-item"><strong>?</strong> <span>Suíte</span></li>
                                                <li class="list-info-item"><strong>{{ $imovel->qtd_vaga_garagem }}</strong> <span>Vaga</span></li>
                                            </ul>
                                        </div>

                                        <div class="group-btn">
                                            <a href="javascript:void(0)" onClick="toggleInfoBox('tel')" class="btn">Ver Telefone</a>
                                            <div class="info-box tel-box">
                                                <span>(99) 9999-9999</span>
                                            </div>
                                            <a href="javascript:void(0)" onClick="toggleInfoBox('msg')" class="btn">Enviar Mensagem</a>
                                            <div class="info-box msg-box">
                                                <form>
                                                    <input type="text" placeholder="Nome">
                                                    <input type="email" placeholder="E-mail">
                                                    <input type="tel" placeholder="Telefone">
                                                    <textarea placeholder="Comentário"></textarea>
                                                    <input type="checkbox" id="email-check">
                                                    <label for="email-check">Desejo receber ofertas por e-mail</label>
                                                    <input type="submit" value="Enviar">
                                                </form>
                                            </div>
                                        </div>

                                        <ul class="list-info light-card">
                                            <li class="list-info-item"><strong>Outras informações</strong> <br><span class="t11">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</span></li>
                                            <li class="list-info-item"><strong>Corretora</strong> <br><span class="t11">Nome da corretora</span></li>
                                            <li class="list-info-item"><strong>código do imóvel</strong> <br><span class="t11">EF87878787</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29378.702148771743!2d-43.483111081508184!3d-23.011367205796113!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9bc2aa587258b1%3A0x94154e3658afd3a!2sRecreio+dos+Bandeirantes%2C+Rio+de+Janeiro+-+RJ!5e0!3m2!1spt-BR!2sbr!4v1548338658541" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>

        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="offset-0 offset-sm-1 col-12 col-sm-10">
                        <div class="wrap-relac">
                            <h2 class="display-title text-center">Ofertas similares</h2>
                            <div class="container-fluid">
                                <div class="list-imovel list-relac row justify-content-between format-col">
                                    <div class="list-item">
                                        <div class="imovel-content">
                                            <div class="container-fluid">
                                                <div class="format-item row">
                                                    <div class="col-5 px-1">
                                                        <div class="relative">
                                                            <div class="thumb-list">
                                                                <a href="{{route('imovel', 1)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('img/imovel-2.jpg')}}');"></div>
                                                                </a>
                                                                <a href="{{route('imovel', 1)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('img/imovel-2.jpg')}}');"></div>
                                                                </a>
                                                            </div>
                                                            <span class="thumb-tag">Destaque!</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <a href="{{route('imovel', 1)}}">
                                                            <h2>Recreio dos Bandeirantes</h2>

                                                            <div class="address">Rua Coronel João Olímpio, 578 - Recreio, Rio de Janeiro - RJ</div>

                                                            <ul class="list-inline">
                                                                <li class="list-inline-item"><strong>3</strong> <span>Quartos</span></li>
                                                                <li class="list-inline-item"><strong>1</strong> <span>Banheiro</span></li>
                                                                <li class="list-inline-item"><strong>1</strong> <span>Vaga</span></li>
                                                            </ul>

                                                            <div class="description">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                            </div>
                                                        </a>

                                                        <div class="bottom">
                                                            <div class="container-fluid">
                                                                <div class="row justify-content-between align-items-center">
                                                                    <div class="col-auto format-nopad">
                                                                        <p class="price">A partir de <br><strong>R$ 99.999</strong></p>
                                                                    </div>
                                                                    <div class="col-auto format-nopad">
                                                                        <a href="{{route('imovel', 1)}}" class="btn">Telefone</a>
                                                                        <a href="{{route('imovel', 1)}}" class="btn">Mensagem</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-item">
                                        <div class="imovel-content">
                                            <div class="container-fluid">
                                                <div class="format-item row">
                                                    <div class="col-5 px-1">
                                                        <div class="relative">
                                                            <div class="thumb-list">
                                                                <a href="{{route('imovel', 1)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('img/imovel-2.jpg')}}');"></div>
                                                                </a>
                                                                <a href="{{route('imovel', 1)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('img/imovel-2.jpg')}}');"></div>
                                                                </a>
                                                            </div>
                                                            <span class="thumb-tag">Destaque!</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <a href="{{route('imovel', 1)}}">
                                                            <h2>Recreio dos Bandeirantes</h2>

                                                            <div class="address">Rua Coronel João Olímpio, 578 - Recreio, Rio de Janeiro - RJ</div>

                                                            <ul class="list-inline">
                                                                <li class="list-inline-item"><strong>3</strong> <span>Quartos</span></li>
                                                                <li class="list-inline-item"><strong>1</strong> <span>Banheiro</span></li>
                                                                <li class="list-inline-item"><strong>1</strong> <span>Vaga</span></li>
                                                            </ul>

                                                            <div class="description">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                            </div>
                                                        </a>

                                                        <div class="bottom">
                                                            <div class="container-fluid">
                                                                <div class="row justify-content-between align-items-center">
                                                                    <div class="col-auto format-nopad">
                                                                        <p class="price">A partir de <br><strong>R$ 99.999</strong></p>
                                                                    </div>
                                                                    <div class="col-auto format-nopad">
                                                                        <a href="{{route('imovel', 1)}}" class="btn">Telefone</a>
                                                                        <a href="{{route('imovel', 1)}}" class="btn">Mensagem</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-item">
                                        <div class="imovel-content">
                                            <div class="container-fluid">
                                                <div class="format-item row">
                                                    <div class="col-5 px-1">
                                                        <div class="relative">
                                                            <div class="thumb-list">
                                                                <a href="{{route('imovel', 1)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('img/imovel-2.jpg')}}');"></div>
                                                                </a>
                                                                <a href="{{route('imovel', 1)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('img/imovel-2.jpg')}}');"></div>
                                                                </a>
                                                            </div>
                                                            <span class="thumb-tag">Destaque!</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <a href="{{route('imovel', 1)}}">
                                                            <h2>Recreio dos Bandeirantes</h2>

                                                            <div class="address">Rua Coronel João Olímpio, 578 - Recreio, Rio de Janeiro - RJ</div>

                                                            <ul class="list-inline">
                                                                <li class="list-inline-item"><strong>3</strong> <span>Quartos</span></li>
                                                                <li class="list-inline-item"><strong>1</strong> <span>Banheiro</span></li>
                                                                <li class="list-inline-item"><strong>1</strong> <span>Vaga</span></li>
                                                            </ul>

                                                            <div class="description">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                            </div>
                                                        </a>

                                                        <div class="bottom">
                                                            <div class="container-fluid">
                                                                <div class="row justify-content-between align-items-center">
                                                                    <div class="col-auto format-nopad">
                                                                        <p class="price">A partir de <br><strong>R$ 99.999</strong></p>
                                                                    </div>
                                                                    <div class="col-auto format-nopad">
                                                                        <a href="{{route('imovel', 1)}}" class="btn">Telefone</a>
                                                                        <a href="{{route('imovel', 1)}}" class="btn">Mensagem</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-item">
                                        <div class="imovel-content">
                                            <div class="container-fluid">
                                                <div class="format-item row">
                                                    <div class="col-5 px-1">
                                                        <div class="relative">
                                                            <div class="thumb-list">
                                                                <a href="{{route('imovel', 1)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('img/imovel-2.jpg')}}');"></div>
                                                                </a>
                                                                <a href="{{route('imovel', 1)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('img/imovel-2.jpg')}}');"></div>
                                                                </a>
                                                            </div>
                                                            <span class="thumb-tag">Destaque!</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <a href="{{route('imovel', 1)}}">
                                                            <h2>Recreio dos Bandeirantes</h2>

                                                            <div class="address">Rua Coronel João Olímpio, 578 - Recreio, Rio de Janeiro - RJ</div>

                                                            <ul class="list-inline">
                                                                <li class="list-inline-item"><strong>3</strong> <span>Quartos</span></li>
                                                                <li class="list-inline-item"><strong>1</strong> <span>Banheiro</span></li>
                                                                <li class="list-inline-item"><strong>1</strong> <span>Vaga</span></li>
                                                            </ul>

                                                            <div class="description">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                            </div>
                                                        </a>

                                                        <div class="bottom">
                                                            <div class="container-fluid">
                                                                <div class="row justify-content-between align-items-center">
                                                                    <div class="col-auto format-nopad">
                                                                        <p class="price">A partir de <br><strong>R$ 99.999</strong></p>
                                                                    </div>
                                                                    <div class="col-auto format-nopad">
                                                                        <a href="{{route('imovel', 1)}}" class="btn">Telefone</a>
                                                                        <a href="{{route('imovel', 1)}}" class="btn">Mensagem</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-item">
                                        <div class="imovel-content">
                                            <div class="container-fluid">
                                                <div class="format-item row">
                                                    <div class="col-5 px-1">
                                                        <div class="relative">
                                                            <div class="thumb-list">
                                                                <a href="{{route('imovel', 1)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('img/imovel-2.jpg')}}');"></div>
                                                                </a>
                                                                <a href="{{route('imovel', 1)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('img/imovel-2.jpg')}}');"></div>
                                                                </a>
                                                            </div>
                                                            <span class="thumb-tag">Destaque!</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-7">
                                                        <a href="{{route('imovel', 1)}}">
                                                            <h2>Recreio dos Bandeirantes</h2>

                                                            <div class="address">Rua Coronel João Olímpio, 578 - Recreio, Rio de Janeiro - RJ</div>

                                                            <ul class="list-inline">
                                                                <li class="list-inline-item"><strong>3</strong> <span>Quartos</span></li>
                                                                <li class="list-inline-item"><strong>1</strong> <span>Banheiro</span></li>
                                                                <li class="list-inline-item"><strong>1</strong> <span>Vaga</span></li>
                                                            </ul>

                                                            <div class="description">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                            </div>
                                                        </a>

                                                        <div class="bottom">
                                                            <div class="container-fluid">
                                                                <div class="row justify-content-between align-items-center">
                                                                    <div class="col-auto format-nopad">
                                                                        <p class="price">A partir de <br><strong>R$ 99.999</strong></p>
                                                                    </div>
                                                                    <div class="col-auto format-nopad">
                                                                        <a href="{{route('imovel', 1)}}" class="btn">Telefone</a>
                                                                        <a href="{{route('imovel', 1)}}" class="btn">Mensagem</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <form class="form-bottom">
            <h2 class="display-title text-center">Entre em contato</h2>
            <div class="wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="offset-0 offset-sm-1 col-12 col-sm-5">
                            <input type="text" placeholder="Nome">
                            <input type="email" placeholder="E-mail">
                            <input type="tel" placeholder="Telefone">
                        </div>
                        <div class="col-12 col-sm-5">
                            <textarea placeholder="Comentário"></textarea>
                        </div>
                        <div class="offset-0 offset-sm-1 col-12 col-sm-10">
                            <input type="submit" value="Enviar">
                            <input type="checkbox" id="email-check">
                            <label for="email-check">Desejo receber ofertas por e-mail</label>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection