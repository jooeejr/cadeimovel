@extends('layouts.app')
@section('class_body')
    imoveis
@endsection
@section('content')
    <style>
        .slick-list {
            position: relative;
            overflow: hidden;
            display: block;
            margin: 0;
            padding: 0;
            width: 374px;
        }
    </style>
    <div id="app">
        <div class="bread-list">
            <div class="wrapper">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="{{route('home')}}">Início</a></li>
                    <li class="list-inline-item">Imóveis</li>
                </ul>
            </div>
        </div>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-3">
                        <aside>
                            <div class="filter">
                                <span>Editar filtro</span>
                            </div>

                            <form method="post" id="form-filter">
                                @csrf
                                <input type="hidden" name="localidade" value="{{ Request::segment(2) }}">
                                <div class="aside-content">
                                    <span id="close-btn">FECHAR</span>
                                    <h3>Tipo de imóvel</h3>
                                    <ul>
                                        <li><input name="tipo[]" value="Casa" type="checkbox" id="tp-casa"><span class="check"></span><label for="tp-casa">Casa</label></li>
                                        <li><input name="tipo[]" value="Apartamento" type="checkbox" id="tp-apto"><span class="check"></span><label for="tp-apto">Apartamento</label></li>
                                        <li><input name="tipo[]" value="Comercial" type="checkbox" id="tp-com"><span class="check"></span><label for="tp-com">Comercial</label></li>
                                    </ul>

                                    <h3>Status do imóvel</h3>
                                    <ul>
                                        <li><input name="status[]" value="Na Planta" type="checkbox" id="st-planta"><span class="check"></span><label for="st-planta">Na planta</label></li>
                                        <li><input name="status[]" value="Em Construção" type="checkbox" id="st-constr"><span class="check"></span><label for="st-constr">Em construção</label></li>
                                        <li><input name="status[]" value="Pronto para morar" type="checkbox" id="st-pronto"><span class="check"></span><label for="st-pronto">Pronto para morar</label></li>
                                    </ul>

                                    <h3>Preço</h3>
                                    <ul class="group-price">
                                        <li>
                                            <label for="max">Mínimo</label>
                                            <input name="min" type="number" step="0.01" onkeypress="return event.charCode >= 48 && event.charCode <= 57" title="Somente números" id="max" placeholder="0">
                                        </li>
                                        <li>
                                            <label for="min">Máximo</label>
                                            <input name="max" type="number" step="0.01" onkeypress="return event.charCode >= 48 && event.charCode <= 57" title="Somente números" id="min" placeholder="Ilimitado">
                                        </li>
                                    </ul>

                                    <h3>Número de quartos</h3>
                                    <ul class="group-check">
                                        <li><input name="qtd_quartos" value="1" type="radio" id="qt-1"><span class="check"></span><label for="qt-1">1 ou mais</label></li>
                                        <li><input name="qtd_quartos" value="2" type="radio" id="qt-2"><span class="check"></span><label for="qt-2">2 ou mais</label></li>
                                        <li><input name="qtd_quartos" value="3" type="radio" id="qt-3"><span class="check"></span><label for="qt-3">3 ou mais</label></li>
                                        <li><input name="qtd_quartos" value="4" type="radio" id="qt-4"><span class="check"></span><label for="qt-4">4 ou mais</label></li>
                                    </ul>

                                    <h3>Vagas na garagem</h3>
                                    <ul class="group-check">
                                        <li><input name="qtd_vagas" value="1" type="radio" id="vg-1"><span class="check"></span><label for="vg-1">1 ou mais</label></li>
                                        <li><input name="qtd_vagas" value="2" type="radio" id="vg-2"><span class="check"></span><label for="vg-2">2 ou mais</label></li>
                                        <li><input name="qtd_vagas" value="3" type="radio" id="vg-3"><span class="check"></span><label for="vg-3">3 ou mais</label></li>
                                        <li><input name="qtd_vagas" value="4" type="radio" id="vg-4"><span class="check"></span><label for="vg-4">4 ou mais</label></li>
                                    </ul>

                                    <h3>O que busca em um imóvel?</h3>
                                    <ul>
                                        <li>
                                            <input name="keyword" type="search" placeholder="Ex: mobiliado, piscina...">
                                        </li>
                                    </ul>

                                    <ul>
                                        <li>
                                            <button onClick="filter()" type="button" style="background: #eb641c; border: 1px solid #eb641c; color: #fff" class="btn btn-primary form-control">
                                                Aplicar Filtros
                                            </button>
                                        </li>
                                    </ul>
                                    <a href="{{route('home')}}" id="submit-btn">Atualizar</a>
                                </div>
                            </form>

                        </aside>
                    </div>

                    <div class="col-12 col-sm-9">
                        <form action="/imoveis" method="post" class="main-search">
                            @csrf
                            <input value="{{ Request::segment(2) }}" autocomplete="off" type="search" name="local" placeholder="Digite o bairro, cidade ou região">
                        </form>
                        <div class="list-header">
                            <div class="container-fluid">
                                <div class="row justify-content-between">
                                    <div class="result">
                                        <p>Imóveis encontrados: <strong id="total_encontrados">{{ $imoveis->total() }} </strong></p>
                                    </div>
                                    <div class="filter-header">
                                        <div class="container-fluid">
                                            <div class="row align-items-center">
                                                <div class="col-12 col-sm-auto">
                                                    <span>Ordernar por</span>
                                                    <select class="order" onChange="changeOrder(this.value)" name="ordenacao">
                                                        <option value="">Selecione</option>
                                                        {{--<option value="">Relevância</option>--}}
                                                        <option value="best-price">Maior preço</option>
                                                        <option value="lower-price">Menor preço</option>
                                                        <option value="recent">Mais recentes</option>
                                                        <option value="old">Mais antigos</option>
                                                    </select>
                                                </div>

                                                <div class="col-12 col-sm-auto">
                                                    <div class="list-format">
                                                        <a href="javascript:void(0);" onClick="toggleFormat('list');">
                                                            <img src="{{asset('img/ico-list.png')}}">
                                                        </a>
                                                        <a href="javascript:void(0);" onClick="toggleFormat('col');">
                                                            <img src="{{asset('img/ico-col.png')}}">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="container-fluid">
                            <div id="conteudo-listagem" class="list-imovel row">
                                <?php
                                    $cont = 0;
                                ?>
                                @foreach($imoveis AS $im)
                                    <div class="list-item">
                                    <div class="imovel-content">
                                        <div class="container-fluid">
                                            <div class="format-item row">
                                                <div class="col-5 px-1">
                                                    <div class="relative">
                                                        <div class="thumb-list">
                                                            @foreach($imoveis[$cont]['imoveis_photos'] AS $photo)
                                                                <a href="{{route('imovel', $im->id)}}" class="thumb main-thumb">
                                                                    <div class="src" style="background-image: url('{{asset('/storage/'.$photo->filename)}}');"></div>
                                                                </a>
                                                            @endforeach
                                                        </div>
                                                        <span class="thumb-tag">Destaque!</span>
                                                    </div>
                                                </div>
                                                <div class="col-7">
                                                    <a href="{{route('imovel', $im->id)}}">
                                                        <h2>{{ $im->titulo }}</h2>

                                                        <div class="address">{{ $im->endereco }}, {{ $im->numero }} - {{ $im->bairro }} - {{ $im->cidade }}</div>

                                                        <ul class="list-inline">
                                                            <li class="list-inline-item"><strong>{{ $im->qtd_quartos }}</strong> <span>Quartos</span></li>
                                                            <li class="list-inline-item"><strong>{{ $im->qtd_banheiros }}</strong> <span>Banheiro</span></li>
                                                            <li class="list-inline-item"><strong>{{ $im->qtd_vaga_garagem }}</strong> <span>Vaga</span></li>
                                                        </ul>

                                                        <div class="description">
                                                            <p>{{ $im->descricao }}</p>
                                                        </div>
                                                    </a>

                                                    <div class="bottom">
                                                        <div class="container-fluid">
                                                            <div class="row justify-content-between align-items-center">
                                                                <div class="col-auto format-nopad">
                                                                    <p class="price">A partir de <br><strong>R$ {{ $im->preco }}</strong></p>
                                                                </div>
                                                                <div class="col-auto format-nopad">
                                                                    <a href="{{route('imovel', $im->id)}}" class="btn">Telefone</a>
                                                                    <a href="{{route('imovel', $im->id)}}" class="btn">Mensagem</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <?php
                                        $cont++;
                                    ?>
                                @endforeach

                                @if($cont == 0)
                                    <div id='alerta-error' class='alert alert-warning'>
                                        <strong>Ooooops!</strong> Nenhum resultado foi encontrado.
                                    </div>
                                @endif
                            </div>

                            <div id="paginacao" class="pagger-list pull-right">
                                {{ $imoveis->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection