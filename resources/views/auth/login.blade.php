@extends('layouts.login')

@section('content')
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">{{ __('Login') }}</div>
            <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="email" id="inputEmail" name="email" class="form-control"
                                   placeholder="Endereço de Email" required="required" autofocus="autofocus">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                            <label for="inputEmail">Endereço de Email</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="password" id="inputPassword" name="password" class="form-control"
                                   placeholder="Senha" autocomplete="password" required="required">
                            @if ($errors->has('password'))
                                <span  role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                            <label for="inputPassword">Senha</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me" name="remember">
                                Lembre-se da senha
                            </label>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary  btn-block">
                        {{ __('Login') }}
                    </button>

                </form>
                <div class="text-center">
                    <a class="d-block small" href="{{route("password.request")}}">Esqueceu a Senha?</a>
                </div>
            </div>
        </div>
    </div>
@endsection