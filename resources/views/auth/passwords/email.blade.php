@extends('layouts.login')

@section('content')
<div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">Redefinir Senha</div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="text-center mb-4">
                    <h4>Esqueceu sua senha?</h4>
                    <p>Digite seu endereço de e-mail e nós lhe enviaremos instruções sobre como redefinir sua senha.</p>
                </div>
                <form method="POST" action="{{route("password.email")}}">
                    @csrf
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Insira o endereço de e-mail" required="required" autofocus="autofocus">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                            <label for="inputEmail">Insira o endereço de e-mail</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Redefinir Senha</button>
                </form>
                <div class="text-center">
                    <a class="d-block small" href="{{route("login")}}">Pagina de Login</a>
                </div>
            </div>
        </div>
    </div>
@endsection
